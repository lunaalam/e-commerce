<?php
/*
* An open source application development framework for PHP
*This is an application that allow the user to add and edit categories and products
*the category has a name and description with multiple products.
*the product has a name,description,image,price, and category.

This class is Responsible for Categories's Actions
as add edit category.

Here is one model for this class which is tha Cat_model
*/
class Cat_con extends CI_Controller
{
	/*
	*constructer fuction loads the model
	*and the ui helper and the session library
	*@return	void
	*@param no parameters
	*/
	public

	function __construct()
	{
		parent::__construct();

	}


/*
*the create function loads the form and set the the form_validation
*rules.
*then call the add() function in "Cat_model.php"
*finally call the "get()" function that also exists in "Cat_model.php"
*to load all the categories in the DB in the "home.php"

*@return	void
*@param no parameters
*/

public function add(){
	$data['cat'] = $this->Cat_model->get();
$this->load->view('all_view/Cat_view/home.php', $data);


}


	public

	function create()
	{
		$this->form_validation->set_rules('name', 'Name','required');
		$this->form_validation->set_rules('des', 'Des','required');

		if ($this->form_validation->run() === FALSE)
		{
			$this->session->set_flashdata('msg', 'error validation');
			return redirect(base_url() . '/Cat_con/add');
		}
		else
		{
			$this->session->set_flashdata('msg', 'added successfully');

			$this->Cat_model->create($_SESSION['id']);
			$data['cat'] = $this->Cat_model->get();
			$this->load->view('all_view/Cat_view/home.php', $data);
		}
	}
	/*
	*the edit function loads the edit form and set the the form_validation
	*rules.
	*then call the "check()" function in "Cat_model.php" to check if the
	*category belongs to this user or not
	*if it is then call the "edit()" function "Cat_model.php"
	*finally call the "get()" function that also exists in "Cat_model.php"
	*to load all the categories in the DB in the "home.php"
	*@return	void
	*@param no parameters
    */

		public function update(){
      $data['cat'] = $this->Cat_model->get();
			$this->load->view('all_view/Cat_view/home.php', $data);

		  }

	public function edit()
	{
		$this->form_validation->set_rules('name','Name', 'trim');
		$this->form_validation->set_rules('des','Des', 'trim');

		if ($this->form_validation->run() === FALSE)
		{
			$this->session->set_flashdata('msg', 'error validation');
			return redirect(base_url() . '/Cat_con/update');
		}
		else
		{
			$data['cat'] = $this->Cat_model->get();
      $ch=$this->Cat_model->check();

      if($ch){
      $res = $this->Cat_model->edit();
       if ($res)
       {
				 $this->session->set_flashdata('msg', 'success');

         $this->load->view('all_view/Cat_view/home.php', $data);
    }

      }
		else {
			$this->session->set_flashdata('msg', 'You are not allowed to upload this category');

       redirect(base_url() . 'Cat_con/update');

			//$this->load->view('all_view/Cat_view/home.php', $data);

		}

		}
	}

}
