<?php
/*
* An open source application development framework for PHP
*This is an application that allow the user to add and edit categories and products
*the category has a name and description with multiple products.
*the product has a name,description,image,price, and category.

This class is Responsible for Products's Actions
as show,edit and add

Here is two models for this class one is the Product model
and the other is for category model

*/
class Prod_con extends CI_Controller

{
	/*
	*constructer fuction loads the two models
	*and the ui helper and the session library
	*@return	void
	*@param no parameters

	*/
	public

	function __construct()
	{
		parent::__construct();

	}


/*
the get function views all the product in agiven name of category
*@return	void
*@param the catname (String)

*/
	public

	function get($var)
	{
		$_SESSION['catname'] = $var;
		$query = $this->Prod_model->show($var);
		$data['pro'] = $query->result();
		$data['numpro'] = $query->num_rows();
		$this->load->view('All_view/Prod_view/product.php', $data);
	}


	/*

	*the create function loads the form (all_view/adpro.php) and set the the form_validation
	*rules.
	*then call the inpro() function in "Prod_model.php"
	*finally call the "show()" function that also exists in "Prod_model.php"
	*to load all the products in the DB in the "product.php"

	*@return	void
	*@param no parameters
	*/

public function create(){
	$catname = $_SESSION['catname'];
	$query = $this->Prod_model->show($catname);
	$data['pro'] = $query->result();
	$data['numpro'] = $query->num_rows();
	$this->load->view('all_view/Prod_view/adpro.php', $data);


}

	public

	function add()
	{

		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('des', 'Des', 'required');
		$this->form_validation->set_rules('price', 'Price', 'required');
		if ($this->form_validation->run() === FALSE)
		{
			$this->session->set_flashdata('msg', 'error validation');
			return redirect(base_url() . '/Prod_con/create');
		}
		else
		{
			$this->session->set_flashdata('msg', 'added successfully');

			$id = $_SESSION['id'];
			$catname = $_SESSION['catname'];
			$re = $this->Prod_model->inpro($id,$catname);
			$query = $this->Prod_model->show($catname);
			$data['pro'] = $query->result();
			$data['numpro'] = $query->num_rows();
				return redirect(base_url() . '/Prod_con/get/'.$catname);

		}
	}
/*
	*the edit function loads the form (all_view/uppro.php) and set the the form_validation
	*rules.
	*befor editing the product call the "check()" that in "Prod_model.php"
	*then call the uppro() function in "Prod_model.php"
	*finally call the "show()" function that also exists in "Prod_model.php"
	*to load all the products in the DB in the "product.php"

	*@return	void
	*@param the product id(int)
	*/

public function edit($id){

	$_SESSION['pid']=$id;
 $data['i'] = $id;
 $data['re']=$this->Prod_model->get($id);

	$this->load->view('all_view/Prod_view/uppro.php', $data);

}
	public

	function update($id)
	{
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('des', 'Des', 'required');
		$this->form_validation->set_rules('price', 'Price', 'required');
     $_SESSION['pid']=$id;
		$data['i'] = $id;
		$data['re']=$this->Prod_model->get($id);

		$ch=$this->Prod_model->check();

		if ($this->form_validation->run() === FALSE)
		{
			$this->session->set_flashdata('msg', 'error validation');
			return redirect(base_url() . '/Prod_con/edit/'.$id);
		}
		else
		{

			if(!$ch){
				$catname = $_SESSION['catname'];

				$this->session->set_flashdata('msg', 'you dont have permission');
	return redirect(base_url() . 'Prod_con/get/'.$catname );

			}
			else {

				$catname = $_SESSION['catname'];
				$query = $this->Prod_model->show($catname);
				$data['pro'] = $query->result();
				$data['numpro'] = $query->num_rows();
				$this->session->set_flashdata('msg', 'success');
				return redirect(base_url() . 'Prod_con/get/'.$catname );

		// $this->load->view('all_view/Prod_view/product.php', $data);

				}


		}

	}
/*
the ret function return from the product page (product.php)
to the category page (home.php)



*/
	public

	function ret()
	{

		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('des', 'Des', 'required');
		if ($this->form_validation->run() === FALSE)
		{
			$data['cat'] = $this->Cat_model->get();
			$this->load->view('all_view/Cat_view/home.php', $data);
		}
		else
		{
			$data['cat'] = $this->Cat_model->get();
			$this->load->view('all_view/Cat_view/home.php', $data);
		}
	}


}

?>
