
<?php
/*

 * An open source application development framework for PHP
*This is an application that allow the user to add and edit categories and products
*the category has a name and description with multiple products.
*the product has a name,description,image,price, and category.

This class is Responsible for User's Actions
as he signup or login

Here is two models for this class one is the user model
and the other is for category model (we  need it cause when the user loged in or signed up we
 have to load the category page )
)

*/


//require_once ('/application/libraries/REST_Controller.php');

class User_con extends CI_Controller

{
	public
/*
*constructer fuction loads the two models
*and the ui helper and the session library
*@return	void
*@param no parameters

*/
	function __construct()
	{
		parent::__construct();
	}


	/*

	*login function loads the login form and set the the form_validation
	rules.
	*and call the "login()" function thats in "User_model.php"
	*if the user in the db then
	call the "get_cat()" (exists in Cat_model class) funtion to load the page of category will load (home.php)

	*@return	void
	*@param no parameters
	*/

	public function login()
{
	$this->load->view('all_view/User_view/login.php');


}




	public function log()
	{

		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		if ($this->form_validation->run() === FALSE)
		{

			return redirect(base_url() . '/User_con/login');
		}
		else
		{

			$sta = $this->User_model->login();
			if ($sta)
			{
				$data['log']=true;


			$_SESSION['id'] = $sta->id;
				$data['cat'] = $this->Cat_model->get();
				$this->load->view('all_view/Cat_view/home.php', $data);
			}
			else

			{
				echo "
				<div class='alert alert-danger'>
		    <strong>Error! </strong>Wrong Email or password
		  </div>
";

$this->load->view('all_view/User_view/login.php');


			}
		}
	}


/*
*signup function loads the signup form and set the the form_validation
rules. (form.php)
*and call "Reg()" function in the "User_model.php"

*@return	void
*@param no parameters

*/

public function regis(){


	$this->load->view('all_view/User_view/form.php');

}


	public function REG()
	{
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		if ($this->form_validation->run() === FALSE)
		{
			return redirect(base_url() . '/User_con/regis');

		}
		else
		{
			$regid=$this->User_model->reg();
        $data['cat'] = $this->Cat_model->get();
        $this->load->view('all_view/Cat_view/home.php',$data);

		}
	}
}

?>
