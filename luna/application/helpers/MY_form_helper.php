<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('method_field')) {
    function method_field($method)
    {
        return '<input type="hidden" name="__method" value="'.$method.'">';
    }
}