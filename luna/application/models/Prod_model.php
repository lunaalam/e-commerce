
<?php
/*
This is a model class to load,add and edit
products



*/
class Prod_model extends CI_Model

{

	/*
the constructer loads the database
and the session libray



	*/
	public

	function __construct()
	{
		$this->load->database();
		$this->load->library('session');
	}


	/*
	show function is responsible to get all products in specified category
	in the data base
	*@return:Array of all products (table of the products)
	*@param: catname(String)

	*/
	public

	function show($var)
	{
		$this->load->helper('url');
		$this->db->select("*");
		$this->db->from("product");
		$this->db->where("cat", $var);
		$query = $this->db->get();
		return $query;
	}

	/*

	*the inpro fuction is responsible to add product
	*with specified name,descripton,cost and image given in "adpro.php" form
	*@retur:the result of insert query
	*@param: userid(int)
	*@param: catanem(String)



	*/




	public function inpro($id, $catname)
	{
		$this->load->helper('url');
		$errors = array();
		$file_name = $_FILES['image']['name']; // ask for name
		$file_size = $_FILES['image']['size']; // ask for size
		$file_tmp = $_FILES['image']['tmp_name']; //the image path
		$file_type = $_FILES['image']['type']; // ask for type
		$extension = explode('.', $file_name); // ask for extention
		$file_ext = strtolower(end($extension));
		$expensions = array(
			"jpeg",
			"jpg",
			"png",
			"gif"
		); // alowed extentions (you can add more , we add it for security)
		if (in_array($file_ext, $expensions) === false)
		{
			$errors[] = 'you are not trying to upload an image -_-';
		}

		if ($file_size > 2097152)
		{ // 2097152KB = 2mb (you can change or remove the size)
			$errors[] = 'big image dude -_-';
		}

		if (empty($errors))
		{
			move_uploaded_file($file_tmp, "../luna/application/assets/img/" . $file_name);
			$data = array(
				'us_id' => $id,
				'cat' => $catname,
				'name' => $this->input->post('name') ,
				'des' => $this->input->post('des') ,
				'price' => $this->input->post('price') ,
				'image' => $file_name
			);
			return $this->db->insert('product', $data);
		} //now we check if the image is uploaded
		else
		{
			foreach($errors as $error)
			{
				echo $error;
			}
		}
	}
	/*

	*the uppro fuction is responsible to update product
	*with specified name,descripton,cost and image given in "uppro.php" form
	*@retur:the result of update query
	*@param: product id(int)




	*/

	public

	function uppro($id)
	{
		



		$this->load->helper('url');
		$errors = array();
		$file_name = $_FILES['image']['name']; // ask for name
		$file_size = $_FILES['image']['size']; // ask for size
		$file_tmp = $_FILES['image']['tmp_name']; //the image path
		$file_type = $_FILES['image']['type']; // ask for type
		$extension = explode('.', $file_name); // ask for extention
		$file_ext = strtolower(end($extension));
		$expensions = array(
			"jpeg",
			"jpg",
			"png",
			"gif"
		); // alowed extentions (you can add more , we add it for security)
		if (in_array($file_ext, $expensions) === false)
		{
			$errors[] = 'you are not trying to upload an image -_-';
		}

		if ($file_size > 2097152)
		{ // 2097152KB = 2mb (you can change or remove the size)
			$errors[] = 'big image dude -_-';
		}

		if (empty($errors))
		{
			move_uploaded_file($file_tmp, "../luna/application/assets/img/" . $file_name);
			$data = array(
				'name' => $this->input->post('name') ,
				'des' => $this->input->post('des') ,
				'price' => $this->input->post('des') ,
				'image' => $file_name
			);
			$this->db->where('id', $id);
			return $this->db->update('product', $data);
		}
	}
  public function check(){
    $this->load->helper('url');


    $id=$_SESSION['id'];
    $pid=$_SESSION['pid'];

    $this->db->select("*");
    $this->db->from("product");
    $this->db->where("id",$pid);

    $query = $this->db->get();
   $re=$query->row();

 if(($re->us_id)==$id)

  return true;
  else return false;


  }
	public function get($id){

		$this->db->select("*");
		$this->db->from("product");
		$this->db->where("id", $id);
		$query = $this->db->get();
		$row = $query->row();

		return $row;



	}

}
