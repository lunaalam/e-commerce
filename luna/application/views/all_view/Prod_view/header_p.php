
<!DOCTYPE html>
<html lang="en">
<head>



    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Portfolio - Dark Admin</title>

    <link rel='stylesheet prefetch' href='https://fonts.googleapis.com/icon?family=Material+Icons'>

    <link rel="stylesheet" href='<?php
echo base_url() . "application/assets/css/bootstrap.minad.css"; ?>'>
    <link rel="stylesheet" href='<?php
echo base_url() . "application/assets/css/local.css"; ?>'>
    <link rel="stylesheet" href='<?php
echo base_url() . "application/assets/js/jquery-1.10.2.min.js"; ?>'>
    <link rel="stylesheet" href='<?php
echo base_url() . "application/assets/js/bootstrap.minad.js"; ?>'>






    <style>
    .button {
      color: #aaa;
      cursor: pointer;
      vertical-align: middle;

    }

    .edit:hover {
      color:blue;
    }

    .plus:hover {
      color: green;
    }

    .delete:hover {
      color: red;
    }

    table, th, td {
      border: 1px solid white;
      border-collapse: collapse;
  }
  th{color:#2c90c6;}
        img {
            filter: gray; /* IE6-9 */
            -webkit-filter: grayscale(1); /* Google Chrome, Safari 6+ & Opera 15+ */
            -webkit-box-shadow: 0px 2px 6px 2px rgba(0,0,0,0.75);
            -moz-box-shadow: 0px 2px 6px 2px rgba(0,0,0,0.75);
            box-shadow: 0px 2px 6px 2px rgba(0,0,0,0.75);
            margin-bottom: 20px;
        }
        img:hover {
            filter: none; /* IE6-9 */
            -webkit-filter: grayscale(0); /* Google Chrome, Safari 6+ & Opera 15+ */
        }
    </style>
</head>
<?php
if ($this->session->flashdata('msg')){?>
<div style="background-color:green;width:800px; margin:0 auto;"><?php echo  $this->session->flashdata('msg')?> </div>
<?php  }?>
