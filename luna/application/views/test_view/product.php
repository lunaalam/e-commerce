
<!DOCTYPE html>
<html lang="en">
<head>



    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Portfolio - Dark Admin</title>

    <link rel='stylesheet prefetch' href='https://fonts.googleapis.com/icon?family=Material+Icons'>

    <link rel="stylesheet" href='<?php echo base_url()."application/assets/css/bootstrap.minad.css"; ?>'>
    <link rel="stylesheet" href='<?php echo base_url()."application/assets/css/local.css"; ?>'>
    <link rel="stylesheet" href='<?php echo base_url()."application/assets/js/jquery-1.10.2.min.js"; ?>'>
    <link rel="stylesheet" href='<?php echo base_url()."application/assets/js/bootstrap.minad.js"; ?>'>






    <style>
    .button {
      color: #aaa;
      cursor: pointer;
      vertical-align: middle;

    }

    .edit:hover {
      color:blue;
    }

    .plus:hover {
      color: green;
    }

    .delete:hover {
      color: red;
    }

    table, th, td {
      border: 1px solid white;
      border-collapse: collapse;
  }
  th{color:#2c90c6;}
        img {
            filter: gray; /* IE6-9 */
            -webkit-filter: grayscale(1); /* Google Chrome, Safari 6+ & Opera 15+ */
            -webkit-box-shadow: 0px 2px 6px 2px rgba(0,0,0,0.75);
            -moz-box-shadow: 0px 2px 6px 2px rgba(0,0,0,0.75);
            box-shadow: 0px 2px 6px 2px rgba(0,0,0,0.75);
            margin-bottom: 20px;
        }
        img:hover {
            filter: none; /* IE6-9 */
            -webkit-filter: grayscale(0); /* Google Chrome, Safari 6+ & Opera 15+ */
        }
    </style>
</head>
<body>

    <div id="wrapper">

      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
          <div class="navbar-header">

              <a class="navbar-brand" href="../../Prod_con/ret">Back to Categories</a>

          </div>
          <div class="collapse navbar-collapse navbar-ex1-collapse">
              <ul class="nav navbar-nav side-nav">
                <a class="navbar-brand" href="../../Prod_con/adpro">Add Product</a>

              </ul>

          </div>
      </nav>


        <div class="container">
            <div class="row">
                <div class="text-center">
                    <h1>Completed Projects</h1>
                </div>
                <div class="row">

              <?php foreach ($pro as $row){?>
                    <div  class="col-md-4">
                        <div  class="well">
                            <img class="thumbnail img-responsive" alt="Bootstrap template" src='<?php echo base_url()."application/assets/img/".$row->image; ?> ' />
                            <span >
                              <table>
                                <thead>
                                  <tr>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Price</th>


                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td><?php echo $row->name ?></td>
                                    <td><?php echo $row->des ?></td>
                                    <td><?php echo $row->price ?></td>
<td>
</td>
                                  </tr>

                                </tbody>

                              </table>
                            </span>
                            <i id="<?php echo $row->id  ?>"  class="material-icons edit" ><a href="../uppro/<?php echo $row->id ?>">edit</a></i>

                        </div>
                    </div>

                    <?php } ?>
                </div>

                    </div>
                </div>
            </div>


</body>
<script>

</script>
</html>
